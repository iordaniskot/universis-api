import _ from 'lodash';
import {TraceUtils, LangUtils} from '@themost/common/utils';
import util from "util";
import Rule from "./../models/rule-model";

/**
 * @class
 * @augments Rule
 */
export default class StudentAttributeRule extends Rule {
    constructor(obj) {
        super('Rule', obj);
    }

    /**
     * Validates an expression against the current student
     * @param {*} obj
     * @param {function(Error=,*=)} done
     */
    validate(obj, done) {
        try {
            const self = this;
            const context = self.context;
            const students = context.model('Student'), student = students.convert(obj.student);
            if (_.isNil(student)) {
                return done(null, self.failure('ESTUD','Student data is missing.'));
            }
            if (_.isNil(student.getId())) {
                return done(null, self.failure('ESTUD','Student data cannot be found.'));
            }
            const op = this.operatorOf();
            if (_.isNil(op)) {
                return done(null, self.failure('EFAIL','An error occurred while trying to validate rules.','The specified operator is not yet implemented.'));
            }
            //get queryable object
            let q = students.where('id').equal(student.getId()).select('id').prepare();
            const fnOperator = q[op];
            if (typeof fnOperator !== 'function') {
                return done(null, self.failure('EFAIL','An error occurred while trying to validate rules.', 'The specified operator cannot be found or is invalid.'));
            }
            q.where(this.checkValues);
            fnOperator.apply(q, [ this.value1, this.value2 ]);
            q.silent().count(function(err, count) {
                if (err) {
                    TraceUtils.error(err);
                    return done(null, self.failure('EFAIL', 'An error occurred while trying to validate rules.'));
                }

                let model = context.model('Student');
                let field = model.field(self.checkValues);
                // check if check value refers to other model e.g. Person
                const checkValue = self.checkValues.split("/");
                let isPersonField = false;
                if (checkValue.length > 1) {
                    // get model of related field
                    field = model.field(checkValue[0]);
                    model = context.model(field.type);
                    field = model.field(checkValue[1]);
                    isPersonField = true;
                }
                // get field value and mapping values
                q = students.where('id').equal(student.getId()).prepare();
                // keep result in data and pass this to validationResult
                const data = {
                    "result": count,
                    "operator": op,
                    "value1": self.value1,
                    "value2": self.value2
                };

                // get student field value
                let selectField =  `${field.name} as name`;
                // check if student field has association
                let parentField= 'name';
                let fieldMapping = model.inferMapping(field.name);
                if (fieldMapping) {
                    // check if student parent field has name or description attribute
                    const parentModel = context.model(fieldMapping.parentModel);
                    if (parentModel.field('name')) {
                        selectField = `${field.name}/name as name`;
                    } else if (parentModel.field('description')) {
                        selectField = `${field.name}/description as name`;
                        parentField = 'description as name';
                    }
                }
                if (isPersonField) {
                    // leave field as it is to query student model
                    selectField = `${self.checkValues} as name`
                }
                // get student field
               q.silent().select(selectField).getItem().then((result) => {
                    if (result) {
                        data.result = result['name'];
                    }
                    //get minimum of prerequisites
                    if (count === 1) {
                        //success
                        if (fieldMapping) {
                            // get description for check values from parentModel
                            self.getValueDescription(fieldMapping, parentField, function (err, message) {
                                return done(null, self.success('SUCC', self.formatMessage(context.__(field.title), self.value1, self.value2), null, data));
                            });
                        } else {
                            return done(null, self.success('SUCC', self.formatMessage(context.__(field.title), self.value1, self.value2), null, data));
                        }
                    }
                    else {
                        //failure
                        if (fieldMapping) {
                            // get description from parentModel
                            self.getValueDescription(fieldMapping,parentField, function(err, message) {
                                done(null, self.failure('FAIL', self.formatMessage(context.__(field.title), self.value1, self.value2), null, data));
                            });
                        }
                        else {
                            done(null, self.failure('FAIL', self.formatMessage(context.__(field.title), self.value1, self.value2), null, data));
                        }
                    }
                }).catch((err) => {
                    //get minimum of prerequisites
                    if (count === 1) {
                        //success
                        return done(null, self.success('SUCC', self.formatMessage(context.__(field.title), self.value1, self.value2),null,data));
                    }
                    else {
                        //failure
                        done(null, self.failure('FAIL',self.formatMessage(context.__(field.title), self.value1, self.value2),null,data));
                    }
                });
          });
        }
        catch(e) {
            done(e);
        }
    }

    getValueDescription(fieldMapping, field, callback) {
    const self = this;
    return self.context.model(fieldMapping.parentModel)
        .where(fieldMapping.parentField).in([self.value1, self.value2])
        .select(fieldMapping.parentField, field).silent().all(function (err, result) {
            if (err)
            {
                return callback();
            }
            if (result && result.length>0)
            {
                self.value1=result[0].name;
                self.value2=result[1] && result[1].name;
            }
            return callback();
        });
    }

    formatMessage(p) {
        const op = LangUtils.parseInt(this.ruleOperator);
        let s;
        switch (op) {
            case 0:s = "Attribute [%s] must be equal to %s"; break;
            case 1:s = "Attribute [%s] must contain '%s'"; break;
            case 2: s ="Attribute [%s] must be different from %s"; break;
            case 3: s = "Attribute [%s] must be greater than %s"; break;
            case 4: s = "Attribute [%s] must be lower than %s"; break;
            case 5: s = "Attribute [%s] must be between %s and %s"; break;
            case 6: s = "Attribute [%s] must start with '%s'"; break;
            case 7: s = "Attribute [%s] must not contain '%s'"; break;
            case 8: s = "Attribute [%s] must be greater or equal to %s"; break;
            case 9: s = "Attribute [%s] must be lower or equal to %s"; break;
        }
        const args = [].slice.call(arguments);
        s= this.context.__(s);
        args.unshift(s);
        for (let i = 0; i < args.length; i++) {
            if (_.isNil(args[i])) {
                args[i]='';
            }
        }
        return util.format.apply(this,args);
    }

    formatDescription(callback) {
        const self = this;
        // get check field
        let model = self.context.model('Student');
        let field = model.field(self.checkValues);
        // check if check value refers to other model e.g. Person
        const checkValue = self.checkValues.split("/");
        if (checkValue.length > 1) {
            // get model of related field
            field = model.field(checkValue[0]);
            model = self.context.model(field.type);
            field = model.field(checkValue[1]);
        }
        let message = self.formatMessage(self.context.__(field.title), self.value1, self.value2);
        // get value description
        let parentField = 'name';
        let fieldMapping = model.inferMapping(field.name);

        if (fieldMapping) {
            // check if student parent field has name or description attribute
            const parentModel = self.context.model(fieldMapping.parentModel);
            if (!parentModel.field('name')) {
                if (parentModel.field('description')) {
                    parentField = 'description as name';
                }
            }
            return self.getValueDescription(fieldMapping, parentField, function (err, message) {
                message = self.formatMessage(self.context.__(field.title), self.value1, self.value2);
                return callback(null, message);
            });
        }
        return callback(null, message);
    }
}
