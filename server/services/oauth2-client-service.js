import unirest from 'unirest';
import {URL} from 'url';
import {ApplicationService, HttpError, DataError} from '@themost/common';
/**
 * @class
 */
class OAuth2ClientService extends ApplicationService {
    /**
     * @param {ExpressDataApplication} app
     */
    constructor(app) {
        super(app);
        /**
         * @name OAuth2ClientService#settings
         * @type {{server_uri:string,token_uri?:string}}
         */
         Object.defineProperty(this, 'settings', {
            writable: false,
            value: app.getConfiguration().getSourceAt("settings/auth"),
            enumerable: false,
            configurable: false
        });
    }

    /**
     * Gets keycloak server root
     * @returns {string}
     */
     getServer() {
        return this.settings.server_uri;
    }

    /**
     * Gets keycloak server root
     * @returns {string}
     */
     getAdminRoot() {
        return this.settings.admin_uri;
    }

    // noinspection JSUnusedGlobalSymbols
    /**
     * Gets user's profile by calling OAuth2 server profile endpoint
     * @param {ExpressDataContext} context
     * @param {string} token
     */
    getProfile(context, token) {
        return new Promise((resolve, reject) => {
            return unirest.get(new URL('me', this.getServer()))
                .header('Accept', 'application/json')
                .query({
                    "access_token":token,
                    "fields":"id,name"
                }).end(function (response) {
                    const responseBody =  response.body;
                    if (responseBody && responseBody.error) {
                        return reject(Object.assign(new HttpError(responseBody.code || 500), responseBody));
                    }
                    return resolve(responseBody);
                });
        });
    }

    // noinspection JSUnusedGlobalSymbols
    /**
     * Gets the token info of the current context
     * @param {ExpressDataContext} context
     */
    getContextTokenInfo(context) {
        if (context.user == null) {
            return Promise.reject(new Error('Context user may not be null'));
        }
        if (context.user.authenticationType !== 'Bearer') {
            return Promise.reject(new Error('Invalid context authentication type'));
        }
        if (context.user.authenticationToken == null) {
            return Promise.reject(new Error('Context authentication data may not be null'));
        }
        return new Promise((resolve, reject) => {
            //decrypt token
            const token = context.user && context.user.authenticationToken;
            return unirest.post(new URL('tokeninfo', this.getServer()))
                .header('Accept', 'application/json')
                .query({
                    "access_token":token
                }).end(function (response) {
                    const responseBody =  response.body;
                    if (responseBody && responseBody.error) {
                        return reject(Object.assign(new HttpError(responseBody.code || 500), responseBody));
                    }
                    return resolve(responseBody);
                });
        });
    }
    /**
     * Gets token info by calling OAuth2 server endpoint
     * @param {ExpressDataContext} context
     * @param {string} token
     */
    getTokenInfo(context, token) {
        return new Promise((resolve, reject) => {
            return unirest.post(new URL('tokeninfo', this.getServer()))
                .header('Accept', 'application/json')
                .query({
                    "access_token":token
                }).end(function (response) {
                    const responseBody =  response.body;
                    if (responseBody && responseBody.error) {
                        return reject(Object.assign(new HttpError(responseBody.code || 500), responseBody));
                    }
                    return resolve(responseBody);
                });
        });
    }

    /**
     * @param {AuthorizeUser} authorizeUser
     */
     authorize(authorizeUser) {
        const tokenURL = this.settings.token_uri ? new URL(this.settings.token_uri) : new URL('authorize', this.getServer());
        return new Promise((resolve, reject)=> {
            return unirest.post(tokenURL)
                .type('form')
                .send(authorizeUser).end(function (response) {
                    if (response.error) {
                        if (response.clientError) {
                            return reject(new HttpError(response.error.status));
                        }
                        return reject(response.error);
                    }
                    return resolve(response.body);
                });
        });
    }

    /**
     * Gets a user by name
     * @param {*} user_id 
     * @param {AdminMethodOptions} options 
     */
     getUserById(user_id, options) {
        return new Promise((resolve, reject) => {
            return unirest.get(new URL(`users/${user_id}`, this.getAdminRoot()))
                .header('Authorization', `Bearer ${options.access_token}`)
                .end(function (response) {
                    if (response.error) {
                        if (response.clientError) {
                            return reject(new HttpError(response.error.status));
                        }
                        return reject(response.error);
                    }
                    return resolve(response.body);
                });
        });
    }

    /**
     * Gets a user by name
     * @param {string} username 
     * @param {AdminMethodOptions} options 
     */
    getUser(username, options) {
        return new Promise((resolve, reject)=> {
            return unirest.get(new URL('users', this.getAdminRoot()))
                .header('Authorization', `Bearer ${options.access_token}`)
                .query({
                    '$filter': `name eq '${username}'`
                })
                .end(function (response) {
                    if (response.error) {
                        if (response.clientError) {
                            return reject(new HttpError(response.error.status));
                        }
                        return reject(response.error);
                    }
                    return resolve(response.body.value && response.body.value[0]);
                });
        });
    }

    /**
     * Gets a user by email address
     * @param {string} email 
     * @param {AdminMethodOptions} options 
     */
     getUserByEmail(email, options) {
        return new Promise((resolve, reject)=> {
            return unirest.get(new URL('users', this.getAdminRoot()))
                .header('Authorization', `Bearer ${options.access_token}`)
                .query({
                    '$filter': `alternateName eq '${email}'`
                })
                .end(function (response) {
                    if (response.error) {
                        if (response.clientError) {
                            return reject(new HttpError(response.error.status));
                        }
                        return reject(response.error);
                    }
                    return resolve(response.body.value && response.body.value[0]);
                });
        });
    }

    /**
     * Updates an existing user
     * @param {*} user 
     * @param {AdminMethodOptions} options 
     */
     updateUser(user, options) {
        return new Promise((resolve, reject)=> {
            if (user.id == null) {
                return reject(new DataError('E_IDENTIFIER', 'User may not be empty at this context.', null, 'User', 'id'));
            }
            const request = unirest.put(new URL(`users/${user.id}`, this.getAdminRoot()));
            return request.header('Authorization', `Bearer ${options.access_token}`)
                .header('Content-Type', `application/json`)
                .send(user)
                .end(function (response) {
                    if (response.error) {
                        if (response.clientError) {
                            return reject(new HttpError(response.error.status));
                        }
                        return reject(response.error);
                    }
                    return resolve(response.body);
                });
        });
    }

    /**
     * Creates a new user
     * @param {*} user 
     * @param {AdminMethodOptions} options 
     */
     createUser(user, options) {
        return new Promise((resolve, reject)=> {
            const request = unirest.post(new URL('users', this.getAdminRoot()));
            return request.header('Authorization', `Bearer ${options.access_token}`)
                .header('Content-Type', `application/json`)
                .send(Object.assign({}, user, {
                    $state: 1 // for create
                }))
                .end(function (response) {
                    if (response.error) {
                        if (response.clientError) {
                            return reject(new HttpError(response.error.status));
                        }
                        return reject(response.error);
                    }
                    return resolve(response.body);
                });
        });
    }

    /**
     * Deletes a user
     * @param {{id: any}} user 
     * @param {AdminMethodOptions} options 
     */
     deleteUser(user, options) {
        return new Promise((resolve, reject)=> {
            if (user.id == null) {
                return reject(new DataError('E_IDENTIFIER', 'User may not be empty at this context.', null, 'User', 'id'));
            }
            const request = unirest.delete(new URL(`users/${user.id}`, this.getAdminRoot()));
            return request.header('Authorization', `Bearer ${options.access_token}`)
                .end(function (response) {
                    if (response.error) {
                        if (response.clientError) {
                            return reject(new HttpError(response.error.status));
                        }
                        return reject(response.error);
                    }
                    return resolve(response.body);
                });
        });
    }
}

export {
    OAuth2ClientService
}

