import { TraceUtils } from '@themost/common';
import { DataObjectState } from '@themost/data';
import { DataConflictError } from '../errors';

/**
 * @param {DataEventArgs} event
 */
async function beforeSaveAsync(event) {
	// operate only on insert
	// note: get state from event target for now because event.state gets altered by DataStateValidator
	if (event.target.$state !== 1 && event.state !== DataObjectState.Insert) {
		return;
	}
	const context = event.model.context;
	const dataModel = context.model(event.model.name);
	// get data model constraints
	const constraints = dataModel.constraints;
	// if there are none, exit
	if (!(constraints && constraints.length)) {
		return;
	}
	// filter unique constraints
	const uniqueConstraints = constraints.filter(
		(constraint) => constraint.type === 'unique'
	);
	// if there are none, exit
	if (!(uniqueConstraints && uniqueConstraints.length)) {
		return;
	}
	// start building and executing queries for each constraint
	// from @themost/data
	for (const constraint of uniqueConstraints) {
		let constraintQuery;
		// build query
		for (let i = 0; i < constraint.fields.length; i++) {
			const attr = constraint.fields[i];
			let value = event.target[attr];
			if (typeof value === 'undefined') {
				continue;
			}
			// check field mapping
			let mapping = event.model.inferMapping(attr);
			if (typeof mapping !== 'undefined' && mapping !== null) {
				if (typeof event.target[attr] === 'object') {
					value = event.target[attr][mapping.parentField];
				}
			}
			if (typeof value === 'undefined') value = null;
			if (constraintQuery) {
				constraintQuery.and(attr).equal(value);
			} else {
				constraintQuery = event.model.where(attr).equal(value);
			}
		}
		if (constraintQuery) {
			// execute query
			const uniqueConstraintViolation = await constraintQuery
				.select('id')
				.silent()
				.count();
			if (uniqueConstraintViolation) {
				throw new DataConflictError(
					constraint.description || 'A unique constraint has been violated.',
					null,
					event.model.name
				);
			}
		}
	}
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
	beforeSaveAsync(event)
		.then(() => {
			return callback();
		})
		.catch((err) => {
			TraceUtils.error(err);
			return callback(err);
		});
}
