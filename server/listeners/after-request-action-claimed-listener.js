/**
 * @param {DataEventArgs} event
 */
import {DataConflictError} from "../errors";

async function afterSaveAsync(event) {
    let previousAgent = null;
    const context = event.model.context;
    if (Object.prototype.hasOwnProperty.call(event.target, 'agent') === false) {
        // do nothing and exit
        return;
    }
    if (event.state === 1) { // insert
        //
    } else if (event.state === 2) { //update
        if (event.previous == null) {
            throw new Error('The previous state of action cannot be determined.');
        }
        // get previous agent
        if (event.previous.agent) {
            previousAgent = await context.model('User').find(event.previous.agent).silent().getItem();
            if (previousAgent == null) {
                throw new DataConflictError('Previous action agent cannot be found or is inaccessible.');
            }
        }
    }
    let currentAgent = null;
    // get current agent
    if (event.target.agent) {
        currentAgent = await context.model('User').find(event.target.agent).silent().getItem();
        if (currentAgent == null) {
            throw new DataConflictError('Action agent cannot be found or is inaccessible.');
        }
    }

    // if agent was set for the first time
    if (previousAgent == null && currentAgent != null) {
        // emit claimed event
        return new Promise((resolve, reject) => {
            // get data object
            const target = event.model.convert(event.target);
            target.emit('claimed', {
                agent: currentAgent
            }, (err) => {
                if (err) {
                    return reject(err);
                }
                return resolve();
            });
        });
    }
    // if agent is empty
    if (previousAgent != null && currentAgent == null) {
        // emit released event
        return new Promise((resolve, reject) => {
            // get data object
            const target = event.model.convert(event.target);
            target.emit('released', {
                agent: previousAgent
            }, (err) => {
                if (err) {
                    return reject(err);
                }
                return resolve();
            });
        });
    }
}
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    return callback();
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    // execute async method
    return afterSaveAsync(event).then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}
