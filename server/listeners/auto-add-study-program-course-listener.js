// eslint-disable-next-line no-unused-vars
import {DataEventArgs} from "@themost/data";
import cloneDeep from 'lodash/cloneDeep';
/**
 * @async
 * @param {DataEventArgs} event
 */
async function beforeSaveAsync(event) {
    if (event.state !== 1) {
        return;
    }
    const context = event.model.context;
    if (event.target.studyProgramCourse == null) {
        return;
    }
    // get study program course
    let studyProgramCourse = context.model('StudyProgramCourse').convert(event.target.studyProgramCourse);
    if (studyProgramCourse.id == null) {
        // find study program course
        const item = await context.model('StudyProgramCourse').where('course').equal(studyProgramCourse.course)
            .and('studyProgram').equal(studyProgramCourse.studyProgram)
            .silent()
            .getItem();
        // if item has been found
        if (item != null) {
            // set identifier
            studyProgramCourse.id = item.id;
        } else {
            // add study program course
            await context.model('StudyProgramCourse').save(studyProgramCourse);
        }
    }

}

async function afterSaveAsync(event) {
    if (event.state !== 1) {
        return;
    }
    const context = event.model.context;
    if (event.target.studyProgramCourse == null) {
        return;
    }
    const target = event.target;
    // check if course is complex and add also courseParts
    const course = await context.model('Course').where('id').equal(event.target.studyProgramCourse.course.id).expand('courseParts').getItem();
    if (course.courseStructureType === 4) {
        // add also courseParts
        const courses = course.courseParts.map((coursePart=>{
            const specializationCourse = cloneDeep(target);
            delete specializationCourse.id;
            delete specializationCourse.studyProgramCourse.id;
            specializationCourse.studyProgramCourse.course = coursePart;
            return specializationCourse;
        }));
        if (courses.length > 0) {
            await context.model('SpecializationCourse').save(courses);
        }
    }
}


async function afterRemoveAsync(event) {
    const context = event.model.context;
    const specializationCourse = event.previous;
    // check if specialization course is the only one and remove also studyProgramCourse
    const exists =await context.model('SpecializationCourse').where('studyProgramCourse').equal(specializationCourse.studyProgramCourse)
        .and('specialization').notEqual(specializationCourse.specialization)
        .count();
    if (!exists) {
        // remove also studyProgramCourse
        await context.model('StudyProgramCourse').remove({id:specializationCourse.studyProgramCourse});
    }
}

export function beforeSave(event, callback) {
    beforeSaveAsync(event).then(() => {
       return callback();
    }).catch((err) => {
        return callback(err);
    });
}


export function afterSave(event, callback) {
    afterSaveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}


export function afterRemove(event, callback) {
    afterRemoveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}
