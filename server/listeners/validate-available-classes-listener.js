import {ApplicationService, TraceUtils} from "@themost/common";
import {DataConfigurationStrategy} from "@themost/data";
import path from "path";
import _ from "lodash";
import {ValidationResult} from "../errors";
import '@themost/promise-sequence';


/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterExecute(event, callback) {
    let data = event['result'];
    // do not validate data in silent mode
    const silentMode = event.emitter.$silent;
    if (silentMode === true) {
        return callback();
    }
    // validate data
    if (data == null) {
        return callback();
    }
    // validate query and exit
    if (event.query.$group) {
        return callback();
    }
    if (!(Array.isArray(data) && data.length))
    {
        return callback();
    }

    const context = event.model.context;
    (async function () {
        // get student department
        const student = await  context.model('Student').select('id','department').where('id').equal(data[0].student).expand('department').silent().getItem();
        if (student == null)
        {
            TraceUtils.warn('ValidateAvailableClassesAction: Student cannot be found.');
            return;
        }
        if (student.department == null)
        {
            TraceUtils.warn('ValidateAvailableClassesAction: Student department cannot be found.');
            return;
        }
        // if validationResult exists, exit listener
        if (data[0].validationResult)
        {
            return;
        }

        let forEachCourse = async availableClass => {
            try {
                const studentCourseClass = context.model('StudentCourseClass').convert(availableClass);
                //add registrationYear and period
                studentCourseClass.registrationYear = student.department.currentYear;
                studentCourseClass.registrationPeriod = student.department.currentPeriod;
                return await new Promise((resolve) => {
                    studentCourseClass.validate(function (err, result) {
                        if (err) {
                            result = _.isNil(result)? [] :result;
                            if (err instanceof ValidationResult) {
                                result.push(err);
                            } else {
                                result.push(new ValidationResult(false, 'EFAIL', context.__('An internal server error occurred.'), err.message));
                            }
                        }
                        availableClass.validationResult=result;
                        return resolve();
                    });
                });
            } catch (err) {
                TraceUtils.error(err);
            }
        };
        // call all promises
        await Promise.sequence(data.map(availableClass => {
            return () => forEachCourse(availableClass);
        }));
    })().then(() => {
       return callback();
    }).catch( err => {
        TraceUtils.error(err);
        return callback();
    });

}



export class ValidateAvailableClassesAction extends ApplicationService {
    /**
     * @param {IApplication} app
     */
    constructor(app) {
        // call super constructor
        super(app);
        // install service
        this.install();
    }

    install() {
        /**
         * get data configuration
         * @type {DataConfigurationStrategy}
         */
        const configuration = this.getApplication().getConfiguration().getStrategy(DataConfigurationStrategy);
        // get StudentRequestAction model definition
        const model = configuration.getModelDefinition('StudentAvailableClass');
        // get this file path relative to application execution path
        const listenerType = './' + path.relative(this.getApplication().getConfiguration().getExecutionPath(), __filename);
        // ensure model event listeners
        model.eventListeners = model.eventListeners || [];
        // try to find event listener
        const findIndex = model.eventListeners.findIndex(listener => {
            return listener.type === listenerType ||
                listener.type === listenerType.replace(/\.js$/, '');
        });
        if (findIndex < 0) {
            // add event listener
            model.eventListeners.push({
                type: listenerType
            });
            // update StudentRequestAction model definition
            configuration.setModelDefinition(model);
            // write to log
            TraceUtils.info('Services: ValidateAvailableClassesAction service has been successfully installed');
        }
    }

    uninstall() {
        /**
         * get data configuration
         * @type {DataConfigurationStrategy}
         */
        const configuration = this.getConfiguration().getStrategy(DataConfigurationStrategy);
        // get StudentRequestAction model definition
        const model = configuration.getModelDefinition('StudentAvailableClass');
        // get this file path relative to application execution path
        const listenerType = './' + path.relative(this.getApplication().getConfiguration().getExecutionPath(), __filename);
        // ensure model event listeners
        model.eventListeners = model.eventListeners || [];
        // try to find event listener
        const findIndex = model.eventListeners.find( listener => {
            return listener.type === listenerType ||
                listener.type === listenerType.replace(/\.js$/, '');
        });
        if (findIndex>=0) {
            // remove event listener
            model.eventListeners.splice(findIndex, 1);
            // update StudentRequestAction model definition
            configuration.setModelDefinition(model);
            // write to log
            TraceUtils.info('Services: ValidateAvailableClassesAction service has been successfully uninstalled');
        }
    }
}
