import {
	DataError,
	DataNotFoundError,
	TraceUtils,
	Guid,
} from "@themost/common";
import { DataObjectState } from "@themost/data";
import { DataConflictError } from "../errors";

class AvailableUsernameFormatElements {
	static availableElements = ['T', 'G', 'F', 'U'];
}


/**
 * @param {DataEventArgs} event
 */
async function afterSaveAsync(event) {
	if (event.state !== DataObjectState.Insert) {
		return;
	}
	const context = event.model.context;
	const instructorId =
		typeof event.target.instructor === "object"
			? event.target.instructor.id
			: event.target.instructor;
	// get instructor
	const instructor = await context
		.model("Instructor")
		.where("id")
		.equal(instructorId)
		.select("id", "familyName", "givenName", "department", "locales", "email")
		.expand(
			"department",
			"locales"
		)
		.getItem();
	if (instructor == null) {
		throw new DataNotFoundError(
			"The specified instructor cannot be found or is inaccessible."
		);
	}
	// validate instructor user
	if (instructor.user != null) {
		throw new DataConflictError(
			`Instructor is already linked to a user with id ${instructor.user}.`
		);
	}
	// get department configuration (avoid caching through Department)
	const departmentConfiguration = await context.model('DepartmentConfiguration')
		.where('department').equal(instructor.department && instructor.department.id)
		.select('id', 'instructorUsernameFormat')
		.silent()
		.getItem();
	// get username format and validate it
	const usernameFormat =
		departmentConfiguration &&
		departmentConfiguration.instructorUsernameFormat;
	if (usernameFormat == null) {
		throw new DataError(
			"E_FORMAT_MISSING",
			`Cannot create user for instructor with id ${instructor.id}, because instructorUsernameFormat is not set for department [${instructor.department.id}]-${instructor.department.name}.`
		);
	}
	// validate format elements
	const validFormatElements = AvailableUsernameFormatElements.availableElements;
	const targetElements = usernameFormat.split(';');
	// target format is valid only if-for every format element-
	// there is an available format element that is equal
	// with the target element's first character
	const targetFormatIsValid = targetElements.every(formatElement => validFormatElements.includes(formatElement[0]));
	if (!targetFormatIsValid) {
		throw new DataConflictError('The instructor username format contains invalid/unsupported format elements.');
	}
	let username;
	try {
		// create new username
		username = createUsername(usernameFormat, instructor);
	} catch (err) {
		TraceUtils.error(err);
		throw new DataError("E_GENERATE_USERNAME", err.message);
	}
	// check if a user with that username already exists
	let exists = await context
		.model("User")
		.where("name")
		.equal(username)
		.select("id")
		.silent()
		.count();
	if (exists) {
		const dynamicElements = ["U"];
		const containsDynamicElements = usernameFormat
			.split(";")
			.find((formatElement) => dynamicElements.includes(formatElement));
		// If the username format does not contain any dynamic elements, a number has to be appended at the end to force uniqueness.
		// This is a safe way to not interfere with the username itself in any way.
		if (!containsDynamicElements) {
			let index = 0;
			do {
				index++;
				exists = await context
					.model("User")
					.where("name")
					.equal(username + index.toString())
					.select("id")
					.silent()
					.count();
			} while (exists);
			// append the final index
			username += index.toString();
		} else {
			// format contains at least one dynamic element, so recreate a username
			do {
				username = createUsername(usernameFormat, instructor);
				exists = await context
					.model("User")
					.where("name")
					.equal(username)
					.select("id")
					.silent()
					.count();
			} while (exists);
		}
	}
	// get instructors group
	const instructorsGroup = await context
		.model("Group")
		.where("name")
		.equal("Instructors")
		.select("id")
		.silent()
		.getItem();
	if (instructorsGroup == null) {
		throw new DataNotFoundError("Instructors group is missing");
	}
	// create a new user and assign instructors group
	const newUser = {
		name: username,
		email: instructor.email,
		description: `${instructor.familyName} ${instructor.givenName}`,
		groups: [
			{
				id: instructorsGroup.id,
			},
		],
		departments: [instructor.department]
	};
	// save new user
	const user = await context.model("UserReference").silent().save(newUser);
	// assign to instructor
	instructor.user = user;
	// and update
	Object.assign(instructor, {
		$state: 2,
	});
	await context.model("Instructor").save(instructor);
	// and finally update (complete) the action
	event.target.actionStatus = {
		alternateName: 'CompletedActionStatus'
	};
	await context
		.model("CreateInstructorUserAction")
		.silent()
		.save(event.target);
}
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
	return afterSaveAsync(event)
		.then(() => {
			return callback();
		})
		.catch((err) => {
			return callback(err);
		});
}

function createUsername(usernameFormat, instructor) {
	let username = usernameFormat;
	let format = usernameFormat.split(";");
	for (let i = 0; i < format.length; i++) {
		const formatElement = format[i];
		// format for plain text
		if (formatElement.startsWith("T")) {
			username = username.replace(formatElement, formatElement.substr(1));
		}
		// format for given name
		if (formatElement.startsWith("G")) {
			// find en locale
			const enLocale = instructor.locales.find(
				(locale) => locale.inLanguage === "en"
			);
			// get given name
			const givenName = enLocale && enLocale.givenName;
			// if it does not exist, throw error
			if (givenName == null) {
				throw new Error(
					`Cannot construct username based on given name for instructor [${instructor.id}]-${instructor.givenName} ${instructor.familyName}, because given name is not en localized.`
				);
			}
			// get length
			const length = parseInt(formatElement.substr(1)) || givenName.length;
			// and replace format element
			username = username.replace(
				formatElement,
				givenName.toLowerCase().substr(0, length)
			);
		}
		// format for family name
		if (formatElement.startsWith("F")) {
			// find en locale
			const enLocale = instructor.locales.find(
				(locale) => locale.inLanguage === "en"
			);
			// get family name
			const familyName = enLocale && enLocale.familyName;
			// if it does not exist, throw error
			if (familyName == null) {
				throw new Error(
					`Cannot construct username based on family name for instructor [${instructor.id}]-${instructor.givenName} ${instructor.familyName}, because family name is not en localized.`
				);
			}
			// get length
			const length = parseInt(formatElement.substr(1)) || familyName.length;
			// and replace format element
			username = username.replace(
				formatElement,
				familyName.toLowerCase().substr(0, length)
			);
		}
		// format for GUID
		if (formatElement.startsWith("U")) {
			username = username.replace(formatElement, Guid.newGuid().toString());
		}
	}
	return username.replace(/;/g, "");
}
