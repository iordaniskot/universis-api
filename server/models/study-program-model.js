import {EdmMapping, EdmType, DataObject, DataPermissionEventListener, DataObjectState} from '@themost/data';
import {promisify} from "es6-promisify";
import {TraceUtils} from "@themost/common/utils";
import { DataConflictError } from '../errors';
const Rule = require('./rule-model');
/**
 * @class
 
 * @property {number} id
 * @property {Department|any} department
 * @property {StudyLevel|any} studyLevel
 * @property {boolean} isActive
 * @property {string} name
 * @property {string} abbreviation
 * @property {string} printName
 * @property {string} degreeDescription
 * @property {GradeScale|any} gradeScale
 * @property {number} decimalDigits
 * @property {boolean} hasFees
 * @property {number} semesters
 * @property {Semester|any} specialtySelectionSemester
 * @property {Array<StudyProgramSpecialty|any>} specialties
 */
@EdmMapping.entityType('StudyProgram')
class StudyProgram extends DataObject {
    /**
     * @constructor
     */
    constructor() {
        super();
    }

    /**
     * Returns a collection of rules which are going to be check while validating that a student is eligible for graduation
     * @returns {Promise<any[]>}
     */
    @EdmMapping.func('GraduationRules', EdmType.CollectionOf('Rule'))
    async getGraduationRules() {
        // return rules of type ProgramCourseRegistrationRule
        return Rule.expand(this.context, this.getId(),
            'Program', 'GraduateRule');
    }

    /**
     * Updates program course registration rules
     * @returns {Promise<any[]>}
     */
    @EdmMapping.param('items', EdmType.CollectionOf('Rule'), false, true)
    @EdmMapping.action('GraduationRules', EdmType.CollectionOf('Rule'))
    async setGraduationRules(items) {
        const finalItems = [];
        for (let item of items) {
            // convert item
            const converted = await Rule.flatten(this.context, item, `${item.refersTo}RuleEx`);
            // set target id
            converted.target = this.getId().toString();
            // set target type
            converted.targetType = 'Program';
            // set additional type
            converted.additionalType = 'GraduateRule';
            finalItems.push(converted);
        }
        // validate permissions
        const validateAsync = promisify(DataPermissionEventListener.prototype.validate);
        await validateAsync({
            model: this.getModel(), // set current model
            state: DataObjectState.Update, // set state to update
            target: this // this object is the target object
        });
        // save items
        await this.context.model(Rule).silent().save(finalItems);
        // and return collection of rules
        return this.getGraduationRules();
    }

    /**
     * Returns a collection of rules which are going to be check while validating
     * that a student is eligible to enroll for this program
     * @returns {Promise<any[]>}
     */
    @EdmMapping.func('InscriptionRules', EdmType.CollectionOf('Rule'))
    async getProgramInscriptionRules() {
        // return rules of type ProgramCourseRegistrationRule
        return Rule.expand(this.context, this.getId(),
            'Program', 'ProgramInscriptionRule');
    }

    /**
     * Updates program enrollment rules
     * @returns {Promise<any[]>}
     */
    @EdmMapping.param('items', EdmType.CollectionOf('Rule'), false, true)
    @EdmMapping.action('InscriptionRules', EdmType.CollectionOf('Rule'))
    async setProgramInscriptionRule(items) {
        const finalItems = [];
        for (let item of items) {
            // convert item
            const converted = await Rule.flatten(this.context, item, `${item.refersTo}RuleEx`);
            // set target id
            converted.target = this.getId().toString();
            // set target type
            converted.targetType = 'Program';
            // set additional type
            converted.additionalType = 'ProgramInscriptionRule';
            finalItems.push(converted);
        }
        // validate permissions
        const validateAsync = promisify(DataPermissionEventListener.prototype.validate);
        await validateAsync({
            model: this.getModel(), // set current model
            state: DataObjectState.Update, // set state to update
            target: this // this object is the target object
        });
        // save items
        await this.context.model(Rule).silent().save(finalItems);
        // and return collection of rules
        return this.getProgramInscriptionRules();
    }

    /**
     * Returns a collection of rules which are going to be checked while validating a student internship
     * @returns {Promise<any[]>}
     */
    @EdmMapping.func('InternshipRules', EdmType.CollectionOf('Rule'))
    async getInternshipRules() {
        // return rules of type ProgramCourseRegistrationRule
        return Rule.expand(this.context, this.getId(),
            'Program', 'InternshipRule');
    }

    /**
     * Updates student internship rules
     * @returns {Promise<any[]>}
     */
    @EdmMapping.param('items', EdmType.CollectionOf('Rule'), false, true)
    @EdmMapping.action('InternshipRules', EdmType.CollectionOf('Rule'))
    async setInternshipRules(items) {
        const finalItems = [];
        for (let item of items) {
            // convert item
            const converted = await Rule.flatten(this.context, item, `${item.refersTo}RuleEx`);
            // set target id
            converted.target = this.getId().toString();
            // set target type
            converted.targetType = 'Program';
            // set additional type
            converted.additionalType = 'InternshipRule';
            // add item
            finalItems.push(converted);
        }
        // validate permissions
        const validateAsync = promisify(DataPermissionEventListener.prototype.validate);
        await validateAsync({
            model: this.getModel(), // set current model
            state: DataObjectState.Update, // set state to update
            target: this // this object is the target object
        });
        // save items
        await this.context.model(Rule).silent().save(finalItems);
        // and return new collection
        return this.getInternshipRules();
    }

    /**
     * Returns a collection of rules which are going to be checked while validating a student thesis
     * @returns {Promise<any[]>}
     */
    @EdmMapping.func('ThesisRules', EdmType.CollectionOf('Rule'))
    async getThesisRules() {
        // return rules of type ProgramCourseRegistrationRule
        return Rule.expand(this.context, this.getId(),
            'Program', 'ThesisRule');
    }

    /**
     * Updates student internship rules
     * @returns {Promise<any[]>}
     */
    @EdmMapping.param('items', EdmType.CollectionOf('Rule'), false, true)
    @EdmMapping.action('ThesisRules', EdmType.CollectionOf('Rule'))
    async setThesisRules(items) {
        const finalItems = [];
        for (let item of items) {
            // convert item
            const converted = await Rule.flatten(this.context, item, `${item.refersTo}RuleEx`);
            // set target id
            converted.target = this.getId().toString();
            // set target type
            converted.targetType = 'Program';
            // set additional type
            converted.additionalType = 'ThesisRule';
            // add item
            finalItems.push(converted);
        }
        // validate permissions
        const validateAsync = promisify(DataPermissionEventListener.prototype.validate);
        await validateAsync({
            model: this.getModel(), // set current model
            state: DataObjectState.Update, // set state to update
            target: this // this object is the target object
        });
        // save items
        await this.context.model(Rule).silent().save(finalItems);
        // and return new collection
        return this.getThesisRules();
    }

    /**
     * @returns {StudyProgram}
     */
    @EdmMapping.param("data", "Object", false, true)
    @EdmMapping.action('copy', 'StudyProgram')
    async copy(data) {
        try {
            // check all students
            const action = await this.context.model('CopyProgramAction').silent().save(
                {
                    studyProgram: this.getId()
                });
            await this.copyProgram(this.context, this.getId(), action,data);
            return action;
        } catch (err) {
            throw (err);
        }
    }

    copyProgram(appContext, program, action,data) {
        const app = appContext.getApplication();
        const context = app.createContext();
        context.user = appContext.user;
        (async function () {
            /**
             * get studyProgram
             * @type {StudyProgram}
             */
            const studyProgram = await context.model('StudyProgram')
                .asQueryable()
                .expand('specialties', 'groups', 'calculationRules', ({
                    'name': 'info',
                    'options': {
                        '$expand': 'locales'
                    }
                }))
                .where('id').equal(program).silent().getTypedItem();

            // get program group rules
            if (studyProgram.groups && studyProgram.groups.length) {
                for (let i = 0; i < studyProgram.groups.length; i++) {
                    /**
                     * @type {ProgramGroup}
                     */
                    const programGroup = context.model('ProgramGroup').convert(studyProgram.groups[i]);
                    studyProgram.groups[i].rules = await programGroup.getRegistrationRules();
                }
            }
            // get specialty rules
            if (studyProgram.specialties && studyProgram.specialties.length) {
                for (let i = 0; i < studyProgram.specialties.length; i++) {
                    /**
                     * @type {StudyProgramSpecialty}
                     */
                    const specialty = context.model('StudyProgramSpecialty').convert(studyProgram.specialties[i]);
                    studyProgram.specialties[i].rules = await specialty.getSpecialtyRules();
                }
            }
            // remove attributes
            studyProgram.name = data && data.description ||  studyProgram.name + '-Copy';
            studyProgram.isActive = 1;
            // get program courses complex and simple
            const programCourses = await context.model('SpecializationCourse')
                .where('studyProgramCourse/studyProgram').equal(studyProgram.id).equal(studyProgram.id)
                .and('studyProgramCourse/course/courseStructureType').in([1, 4])
                .expand({
                    'name': 'studyProgramCourse',
                    'options': {
                        '$expand': 'course'
                    }
                }).orderBy('specializationIndex')
                .getAllItems();
            if (programCourses.length) {
                for (let i = 0; i < programCourses.length; i++) {
                    /**
                     * @type {StudyProgramCourse}
                     */
                    const programCourse = context.model('SpecializationCourse').convert(programCourses[i]);
                    programCourses[i].rules = await programCourse.getProgramCourseRegistrationRules();
                }
            }
            studyProgram.programCourses = programCourses;
            studyProgram.inscriptionRules = await studyProgram.getProgramInscriptionRules();
            studyProgram.thesisRules = await studyProgram.getThesisRules();
            studyProgram.internshipRules = await studyProgram.getInternshipRules();
            studyProgram.graduationRules = await studyProgram.getGraduationRules();

            delete studyProgram.id;
            // return new studyProgram
            return await context.model('StudyProgram').save(studyProgram);

        })().then((result) => {
            context.finalize(() => {
                // build result
                // after finishing copying program, update actionStatus
                action.actionStatus = {alternateName: 'CompletedActionStatus'};
                action.endTime = new Date();
                action.result = result;
                return context.model('CopyProgramAction').silent().save(action).then(action => {
                }).catch(err => {
                    TraceUtils.error(err);
                });
            });
        }).catch(err => {
            context.finalize(() => {
                action.actionStatus = {alternateName: 'FailedActionStatus'};
                action.endTime = new Date();
                action.description = err.message;
                return context.model('CopyProgramAction').silent().save(action).then(action => {
                }).catch(err => {
                    TraceUtils.error(`An error occurred while copying study program with id ${program}`);
                    TraceUtils.error(err);
                });
            });
        });
    }

    @EdmMapping.action('delete', 'DeleteProgramAction')
    async delete() {
        try {
            // initiate a delete program action
            return await this.context
                .model('DeleteProgramAction')
                .save({
                    studyProgram: this.getId()
                });
        } catch (err) {
            TraceUtils.error(err);
            throw err;
        }
    }
}
module.exports = StudyProgram;
