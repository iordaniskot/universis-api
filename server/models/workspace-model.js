import { EdmMapping, EdmType, DataObject } from '@themost/data';
import localeCode from 'iso-639-1';
import {TranslateService} from '../services/translate-service';
/**
 * @class

 */
@EdmMapping.entityType('Workspace')
class Workspace extends DataObject {
    /**
     * @constructor
     */
    constructor() {
        super();
    }

    @EdmMapping.func('locales', EdmType.CollectionOf('Object'))
    static async getLocales(context) {
        const locales = context.getConfiguration().getSourceAt('settings/i18n/locales');
        if (locales == null) {
            return [];
        }
        return locales.map((locale) => {
            let name = localeCode.getNativeName(locale);
            if (typeof name === 'string' && name.length === 0) {
                // try to find locale from translations
                name = context.application.getService(TranslateService).get(locale);
            }
            return {
                id: locale,
                name: name
            }
        })
    }

}
module.exports = Workspace;
