import _ from 'lodash';
import {ValidationResult} from "../errors";
import {DataObject} from "@themost/data/data-object";
import {LangUtils, TraceUtils} from '@themost/common/utils';
import {EdmMapping, EdmType} from "@themost/data/odata";
import {DataError} from "@themost/common/errors";
import util from "util";

@EdmMapping.entityType('Rule')
/**
 * @class
 */
class Rule extends DataObject {

    constructor() {
        super();
        Object.defineProperty(this,'minExam',{
            get:function() {
                if (_.isEmpty(this.value5)) { return; }
                const arr = this.value5.split(';');
                if (_.isEmpty(arr[1])) { return; }
                return LangUtils.parseInt(arr[1]);
            }, configurable:false, enumerable:false
        });
        Object.defineProperty(this,'maxExam',{
            get:function() {
                if (_.isEmpty(this.value5)) { return; }
                const arr = this.value5.split(';');
                if (_.isEmpty(arr[0])) { return; }
                return LangUtils.parseInt(arr[0]);
            }, configurable:false, enumerable:false
        });
    }

    /**
     * @param {*} data
     * @param {Function} done
     */
    validate(data, done) {
        done = done || function() {};
        done(null, true);
    }

    /**
     * Returns the comparison operator associated with this rule
     * @returns {string}
     */
    operatorOf() {
        const op = LangUtils.parseInt(this.ruleOperator);
        switch (op) {
            case 0:return 'equal';
            case 1:return 'contains';
            case 2: return 'notEqual';
            case 3: return 'greaterThan';
            case 4: return 'lowerThan';
            case 5: return 'between';
            case 6: return 'startsWith';
            case 7: return 'notContains';
            case 8: return 'greaterOrEqual';
            case 9: return 'lowerOrEqual';
        }
    }

    /**
     *
     * @param {string} code
     * @param {string} message
     * @param {string=} innerMessage
     * @param {any=} data
     * @returns {ValidationResult}
     */
    success(code, message, innerMessage, data) {
        // remove ":" character because it is used at i18n translate function to store translated value and results wrong message if key value exists
        let sMessage = message.replace(/:/g, ' ');
        if (this.context) { sMessage = this.context.__(sMessage); }
        const res = new ValidationResult(true, code, sMessage, innerMessage, data);
        res.id = this.id;
        return res;
    }

    /**
     *
     * @param {string} code
     * @param {string} message
     * @param {string=} innerMessage
     * @param {any} data
     * @returns {ValidationResult}
     */
    failure(code, message, innerMessage, data) {
        // remove ":" character because it is used at i18n translate function to store translated value and results wrong message if key value exists
        let sMessage = message.replace(/:/g, ' ');
        if (this.context) { sMessage = this.context.__(sMessage); }
        const res = new ValidationResult(false, code, sMessage, innerMessage, data);
        res.id = this.id;
        return res;
    }

    /**
     * @param {DataObject|Student} student
     * @param {Function} callback
     */
    excludeStudent(student, callback) {
        const self = this, context = self.context;
        //validate exceptions
        let q1 = context.model('Student').where('id').equal(student.getId()).prepare();
        if (self.minExam && self.maxExam) {
            q1.where('semester').lowerThan(self.minExam).or('semester').greaterThan(self.maxExam).prepare();
        }
        else if (self.minExam) {
            q1.where('semester').lowerThan(self.minExam).prepare();
        }
        else if (self.maxExam) {
            q1.where('semester').greaterThan(self.maxExam).prepare();
        }
        else {
            q1 = null;
        }
        if (_.isNil(q1)) {
            callback(null, false);
        }
        else {
            q1.silent().count(function(err, result) {
                if (err) { return callback(err); }
                callback(null, (result===1));
            });
        }
    }

    formatMessage(p) {
        //
    }

    formatDescription(callback) {
        return callback();
    }

    fnValidateExpression = function(x, validationResults) {
        try {
            let expr = x ['ruleExpression'];
            if (_.isEmpty(expr)) {
                return false;
            }
            expr = expr.replace(/\[%(\d+)]/g, function () {
                if (arguments.length === 0) return;
                const id = parseInt(arguments[1]);
                const v = validationResults.find(function (y) {
                    return y.id === id;
                });
                if (v) {
                    return v.success.toString();
                }
                return 'false';
            });
            expr = expr.replace(/\bAND\b/ig, ' && ').replace(/\bOR\b/ig, ' || ').replace(/\bNOT\b/ig, ' !');
            return eval(expr);
        }
        catch (e) {
            return e;
        }
    };

    fnTitleExpression = function(x, validationResults) {
        try {
            let expr = x ['ruleExpression'];
            if (_.isEmpty(expr)) {
                return false;
            }
            expr = expr.replace(/\[%(\d+)]/g, function () {
                if (arguments.length === 0) return;
                const id = parseInt(arguments[1]);
                const v = validationResults.find(function (y) {
                    return y.id === id;
                });
                if (v) {
                    return '(' + v.message.toString() + ')';
                }
                return 'Unknown Rule';
            });
            expr = expr.replace(/\bAND\b/ig, this.context.__(' AND ')).replace(/\bOR\b/ig, this.context.__(' OR ')).replace(/\bNOT\b/ig, this.context.__(' NOT '));
            return expr;
        }
        catch (e) {
            return e;
        }
    };


    /**
     *
     * @param {string} targetModel
     */
    async expand(targetModel) {
        let result = {};
        const model = this.context.model(targetModel);

        const attributes = model.attributes;
        for (let i = 0; i < attributes.length; i++) {
            const attribute = attributes[i];
            const mapping = model.inferMapping(attribute.name);
            let value;
            // get target model property name
            let name = attribute.property || attribute.name;
            if (mapping == null) {
                if (this[attribute.name] != null) {
                    Object.defineProperty(result, name, {
                        configurable: true, enumerable: true, writable: true, value: this[attribute.name]
                    });
                }
            } else {
                value = mapping.associationType === 'junction' ? [] : null;
                if (this[attribute.name] != null) {
                    if (mapping.associationType === 'junction') {
                        value = [];
                        if (mapping.parentModel === model.name) {
                            if (this[attribute.name] === "-1") {
                                value.push({
                                    "id": -1
                                });
                            } else {
                                const values = this[attribute.name].split(',');
                                value = await this.context.model(mapping.childModel).where(mapping.childField).in(values).getAllItems();
                            }
                        }
                    }
                    if (mapping.associationType === 'association') {
                        value = await this.context.model(mapping.parentModel).where(mapping.parentField).equal(this[attribute.name]).getItem();
                    }
                    Object.defineProperty(result, name, {
                        configurable: true, enumerable: true, writable: true, value: value
                    });
                }
            }
        }
        return result;
    }

   static async flatten(context, object , modelName)
    {
        let result = {};
        const model = context.model(modelName);
        // set object state (if it is defined)
        if (Object.prototype.hasOwnProperty.call(object, '$state')) {
            Object.assign(result, {
                $state: object.$state
            });
        }
        const attributes = model.attributes;
        for (let i = 0; i < attributes.length; i++) {
            const attribute = attributes[i];
            const mapping = model.inferMapping(attribute.name);
            let value;
            // get attribute name
            let name = attribute.property || attribute.name;
            if (mapping == null) {
                // get attribute value
                value = object[name];
                // if value is not empty
                if (value != null) {
                    // set value
                    Object.defineProperty(result, attribute.name, {
                        configurable: true, enumerable: true, writable: true, value: value
                    });
                }
            } else {
                value = mapping.associationType === 'junction' ? [] : null;
                // if object property exists
                if (object[name] != null) {
                    if (mapping.associationType === 'junction') {
                        value = null;
                        if (mapping.parentModel === model.name) {
                            let values = object[name];
                            if (Array.isArray(values) && values.length>0) {
                                values = values.filter(value => {
                                    return Object.keys(value).length !== 0;
                                });
                                value = values.length > 0 ? values.map(function (x) {
                                    return x[mapping.childField];
                                }).join(',') : null;
                            }
                        }
                    } else if (mapping.associationType === 'association') {
                        // if mapping foreign field is not defined
                        if (Object.prototype.hasOwnProperty.call(object[name], mapping.parentField) === false) {
                            // get parent object
                            const parentObject = await context.model(mapping.parentModel).find(object[name]).getItem();
                            if (parentObject == null) {
                                throw new DataError('E_ASSOCIATION_OBJECT','An associated object cannot be found or is inaccessible.', null, attribute.model, attribute.name);
                            }
                            // and foreign field value
                            value = parentObject[mapping.parentField];
                        } else {
                            // otherwise get foreign field value
                            value = object[name][mapping.parentField];
                        }
                    } else {
                        throw new DataError('E_ASSOCIATION_TYPE','Unknown or unsupported association type.', null, attribute.model, attribute.name);
                    }
                    // finally set property
                    Object.defineProperty(result, attribute.name, {
                        configurable: true, enumerable: true, writable: true, value: value
                    });
                }
            }
        }
        return result;
    }

    static async expand(context, target,targetType,additionalType) {

        const result = [];
        // get rules
        const entityType = this.entityTypeDecorator;
        const rules = await context.model(entityType)
            .where('targetType').equal(targetType)
            .and('target').equal(target.toString())
            .and('additionalType').equal(additionalType)
            .getAllItems();
        // expand rules
        for (let i = 0; i < rules.length; i++) {
            const model = `${rules[i].refersTo}Rule`;
            /**
             * {Rule}
             */
            const rule = context.model(model).convert(rules[i]);
            // expand rule
            const expanded = await rule.expand(`${rule.refersTo}RuleEx`);
            // add description for each rule
            try {
                const formatDescriptionAsync = util.promisify(rule.formatDescription).bind(rule);
                expanded.description = await formatDescriptionAsync();
            }
            catch (err) {
                TraceUtils.error(`Getting rule description [${rule.id}  failed - ${err.message}`);
            }
            result.push(expanded);
        }
        return result;
    }

    @EdmMapping.param('data','Object', false, true)
    @EdmMapping.action('copyRules', EdmType.CollectionOf('Rule'))
    static async copyRules(context, data) {

        const fromObject = _.isObject(data.source) ? data.source.id : data.source;
        const additionalType = data.additionalType;
        const targetType = data.targetType;
        const toObject = _.isObject(data.destination) ? data.destination.id : data.destination;
        if (fromObject == null || toObject == null || additionalType == null || targetType == null) {
            throw new DataError('E_INVALID_DATA', 'Missing required parameters', null, 'Rule', 'Rule');
        }
        // check if destination object exists
        const exists = await context.model(targetType).where('id').equal(toObject).getItem();
        if (!exists) {
            throw new DataError('E_INVALID_DATA', 'Destination object does not exist or is unavailable', null, 'Rule', 'Rule');
        }
        let rules = await context.model('Rule').where('target').equal(fromObject.toString())
            .and('additionalType').equal(additionalType)
            .and('targetType').equal(targetType).silent().getItems();

        if (rules && rules.length > 0) {
            // clear rules of destination object
            // remove existingRules if any
            const existingRules = await context.model('Rule').where('target').equal(toObject.toString())
                .and('additionalType').equal(additionalType)
                .and('targetType').equal(targetType).silent().getItems();
            existingRules.map(x => {
                x.$state = 4;
            });

            // check if source object has complex rules
            const hasComplexRules = rules.find(x => {
                return x.ruleExpression != null;
            });
            if (hasComplexRules) {
                // ruleExpression should be replaced from new ids
                let ruleExpression = rules[0].ruleExpression;
                let newRules = [];
                //each rule should be saved to get new id and replace it to ruleExpression
                for (let i = 0; i < rules.length; i++) {
                    const rule = rules[i];
                    rule.target = toObject;
                    const oldId = rule.id;
                    delete rule.id;
                    // save new rule
                    const result = await context.model('Rule').silent().save(rule);
                    newRules.push(result);
                    ruleExpression = ruleExpression.replace(new RegExp(`\\[%${oldId}\\]`, 'g'), `[%${result.id}]`);
                }
                // update ruleExpression
                newRules = newRules.map(x => {
                    x.ruleExpression = ruleExpression;
                    return x;
                });
                // save rules
                if (existingRules && existingRules.length > 0) {
                    newRules.push.apply(newRules, existingRules);
                }
                return await context.model('Rule').silent().save(newRules);
            } else {
                // remove id and save
                rules.map(x => {
                    delete x.id;
                    x.target = toObject;
                });
                if (existingRules && existingRules.length > 0) {
                    rules.push.apply(rules, existingRules);
                }
                return await context.model('Rule').silent().save(rules);
            }
        }
    }
}

module.exports = Rule;
