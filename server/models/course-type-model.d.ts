import {EdmMapping,EdmType} from '@themost/data/odata';

import {DataObject} from '@themost/data/data-object';
/**
 * @class
 */
declare class CourseType extends DataObject {

     
     /**
     * @description Κωδικός
     */
    public id: number;

    /**
     * @description Η ονομασία του αντικειμένου
     */
    public name: string;

    /**
     * @description Συντομογραφία
     */
    public abbreviation: string;

}

export = CourseType;