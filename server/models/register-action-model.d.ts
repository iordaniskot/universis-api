import {EdmMapping,EdmType} from '@themost/data/odata';
import InteractAction = require('./interact-action-model');

/**
 * @class
 */
declare class RegisterAction extends InteractAction {

     
     /**
      * @description Μοναδικός κωδικός
      */
     public id: number; 

}

export = RegisterAction;