import {DataObject, DataObjectState, DataPermissionEventListener, EdmMapping, EdmType} from "@themost/data";
import {promisify} from 'util';
const Rule = require('./rule-model');
import _ from 'lodash';
import {ValidationResult} from "../errors";
import util from "util";
import { TraceUtils} from '@themost/common/utils';
import '@themost/promise-sequence';


/**
 * @class
 */
@EdmMapping.entityType('CourseClassSection')
class CourseClassSection extends DataObject {
    /**
     * @constructor
     */
    constructor() {
        super();
    }

    /**
     * Returns a collection of rules which are going to be validated during course class section registration
     * @returns {Promise<any[]>}
     */
    @EdmMapping.func('CourseClassSectionRules', EdmType.CollectionOf('CourseClassSectionRule'))
    async getCourseClassSectionRules() {
            // use custom expand method because CourseClassSectionRule *implements* rule and does not inherit it.
            // so in the model 'Rule' results for this query will always be empty.
            return this.expand(this.context, this.getId(),
                'CourseClassSection', 'ClassRegistrationRule');
    }

    /**
     * Updates course class registration rules
     * @returns {Promise<any[]>}
     */
     @EdmMapping.param('items', EdmType.CollectionOf('CourseClassSectionRule'), false, true)
     @EdmMapping.action('CourseClassSectionRules', EdmType.CollectionOf('CourseClassSectionRule'))
     async setCourseClassSectionRules(items) {
         const finalItems = [];
         for(let item of items) {
             // convert item
             const converted = await Rule.flatten(this.context, item, `${item.refersTo}RuleEx`);
             // set target id
             converted.target = this.getId().toString();
             // set target type
             converted.targetType = 'CourseClassSection';
             // set additional type
             converted.additionalType = 'ClassRegistrationRule';
             finalItems.push(converted);
         }
         // validate permissions
         const validateAsync = promisify(DataPermissionEventListener.prototype.validate);
         await validateAsync({
             model: this.getModel(), // set current model
             state: DataObjectState.Update, // set state to update
             target: this // this object is the target object
         });
         // save items
         await this.context.model('CourseClassSectionRule').silent().save(finalItems);
         // and return new collection
         return this.getCourseClassSectionRules();
    }

    async expand(context, target,targetType,additionalType) {
        const result = [];
        // get rules
        const rules = await context.model('CourseClassSectionRule')
            .where('targetType').equal(targetType)
            .and('target').equal(target.toString())
            .and('additionalType').equal(additionalType)
            .getAllItems();
        // expand rules
        for (let i = 0; i < rules.length; i++) {
            const model = `${rules[i].refersTo}Rule`;
            /**
             * {Rule}
             */
            const rule = context.model(model).convert(rules[i]);
            // expand rule
            const expanded = await rule.expand(`${rule.refersTo}RuleEx`);
            // add description for each rule
            try {
                const formatDescriptionAsync = util.promisify(rule.formatDescription).bind(rule);
                expanded.description = await formatDescriptionAsync();
            }
            catch (err) {
                TraceUtils.error(`Getting rule description [${rule.id}  failed - ${err.message}`);
            }

            result.push(expanded);
        }
        return result;
    }

    async validate(student) {
        const context = this.context;
        const st = await context.model('Student').where("id").equal(student).silent().getTypedItem();
        const data = {
            student: st
        };
        // validate program group rules
        const rules = await context.model('CourseClassSectionRule').where('additionalType').equal('ClassRegistrationRule')
            .and('targetType').equal('CourseClassSection')
            .and('target').equal(this.getId().toString()).getItems();

        if (rules && rules.length===0) {
            return;
        }
        let validationResults = [];
        // add async function for validating section registration rules
        let forEachRule = async groupRule => {
            try {
                return await new Promise((resolve) => {
                    const ruleModel = context.model(groupRule.refersTo + 'Rule');
                    if (_.isNil(ruleModel)) {
                        validationResults = validationResults || [];
                        validationResults.push(new ValidationResult(false, 'FAIL', context.__('Student section registration rule type cannot be found.')));
                    }
                    const rule = ruleModel.convert(groupRule);
                    rule.validate(data, function (err, result) {
                        if (err) {
                            validationResults = validationResults || [];
                            validationResults.push(new ValidationResult(false, 'FAIL', context.__('Student section registration rule type cannot be found.')));
                        }
                        /**
                         * @type {ValidationResult[]}
                         */
                        validationResults = validationResults || [];
                        validationResults.push(result);
                        groupRule.validationResult = result;
                        return resolve();
                    });
                });
            } catch (err) {
                TraceUtils.error(err);
            }
        };
        // call all promises
        await Promise.sequence(rules.map(rule => {
            return () => forEachRule(rule);
        }));

        const fnValidateExpression = function (x) {
            try {
                let expr = x['ruleExpression'];
                if (_.isEmpty(expr)) {
                    return false;
                }
                expr = expr.replace(/\[%(\d+)]/g, function () {
                    if (arguments.length === 0) return;
                    const id = parseInt(arguments[1]);
                    const v = validationResults.find(function (y) {
                        return y.id === id;
                    });
                    if (v) {
                        return v.success.toString();
                    }
                    return 'false';
                });
                expr = expr.replace(/\bAND\b/ig, ' && ').replace(/\bOR\b/ig, ' || ').replace(/\bNOT\b/ig, ' !');
                return eval(expr);
            } catch (e) {
                return e;
            }
        };

        const fnTitleExpression = function (x) {
            try {
                let expr = x['ruleExpression'];
                if (_.isEmpty(expr)) {
                    return false;
                }
                expr = expr.replace(/\[%(\d+)]/g, function () {
                    if (arguments.length === 0) return;
                    const id = parseInt(arguments[1]);
                    const v = validationResults.find(function (y) {
                        return y.id === id;
                    });
                    if (v) {
                        return '(' + v.message.toString() + ')';
                    }
                    return 'Unknown Rule';
                });
                expr = expr.replace(/\bAND\b/ig, context.__(' AND ')).replace(/\bOR\b/ig, context.__(' OR ')).replace(/\bNOT\b/ig, context.__(' NOT '));
                return expr;
            } catch (e) {
                return e;
            }
        };

        let res, title;

        //apply default expression
        const expr = rules.find(function (x) {
            return !_.isEmpty(x['ruleExpression']);
        });
        let finalResult;
        if (expr) {
            res = fnValidateExpression(expr);
            title = fnTitleExpression(expr);
            finalResult = new ValidationResult(res, res === true ? 'SUCC' : 'FAIL', title);
            finalResult.type = 'CourseClassSectionRule';
            expr.ruleExpression= expr.ruleExpression.replace(/\bAND\b/ig, ' && ').replace(/\bOR\b/ig, ' || ').replace(/\bNOT\b/ig, ' !');
            expr.ruleExpression = expr.ruleExpression.replace(/\[%(\d+)]/g, function () {
                if (arguments.length === 0) return;
                return parseInt(arguments[1]);
            });

        } else {
            //get expression (for this rule type)
            const ruleExp1 = rules.map(function (x) {
                return '[%' + x.id + ']';
            }).join(' AND ');
            res = fnValidateExpression({ruleExpression: ruleExp1});
            title = fnTitleExpression({ruleExpression: ruleExp1});
            finalResult = new ValidationResult(res, res === true ? 'SUCC' : 'FAIL', title);
            finalResult.type = 'CourseClassSectionRule';
            finalResult.expression = null;
        }
        return {finalResult, "courseClassSectionRules": rules};
    }
}
module.exports = CourseClassSection;
