import {EdmMapping,EdmType} from '@themost/data/odata';
import {DataObject} from '@themost/data/data-object';
import Department = require('./department-model');
import CourseArea = require('./course-area-model');
import GradeScale = require('./grade-scale-model');
import Instructor = require('./instructor-model');
import CourseStructureType = require('./course-structure-type-model');
import CourseSector = require('./course-sector-model');
import CourseCategory = require('./course-category-model');

/**
 * @class
 */
declare class Course extends DataObject {

     
     /**
      * @description Μοναδικός κωδικός μαθήματος
      */
     public id: string; 
     
     /**
      * @description Αποτελεί τον κωδικό με τον οποίο το μάθημα εμφανίζεται σε αναλυτικές βαθμολογίες, διαδικτυακούς τόπους κλπ. 
      */
     public displayCode: string; 
     
     /**
      * @description Πρόκειται για το τμήμα του οποίου η Γενική Συνέλευση είναι υπεύθυνη για τον ορισμό της ανάθεσης διδασκαλίας του μαθήματος.
      */
     public department: Department|any; 
     
     /**
      * @description Κωδικός γνωστικού αντικειμένου
      */
     public courseArea?: CourseArea|any; 
     
     /**
      * @description Ο τίτλος του μαθήματος
      */
     public name: string; 
     
     /**
      * @description Ο υπότιτλος του μαθήματος
      */
     public subtitle?: string; 
     
     /**
      * @description Αν το μάθημα προσφέρεται ή έχει καταργηθεί
      */
     public isEnabled: boolean; 
     
     /**
      * @description Πρόκειται για μαθήματα που προσφέρονται και σε άλλα προγράμματα σπουδών άλλων τμημάτων συνήθως ως μαθήματα τύπου ελεύθερης επιλογής. 
      */
     public isShared: boolean; 
     
     /**
      * @description Η πιο διαδεδομένη βαθμολογική κλίμακα είναι η δεκαδική (από 0 έως 10 με βάση το 5), ωστόσο κάποια χρησιμοποιούν και άλλες βαθμολογικές κλίμακες (πχ. Λατινικά γράμματα, Επιτυχώς/Ανεπιτυχώς, δεκαδική από 0 έως 10 με βάση το 6 κ.α.) 
      */
     public gradeScale?: GradeScale|any; 
     
     /**
      * @description Ο επιστημονικά υπεύθυνος του μαθήματος
      */
     public instructor?: Instructor|any; 
     
     /**
      * @description Αν το μάθημα υπολογίζεται στις υποτροφίες (ΝΑΙ/ΟΧΙ)
      */
     public isCalculatedInScholarship?: boolean; 
     
     /**
      * @description Οι διδακτικές μονάδες του μαθήματος
      */
     public units?: number; 
     
     /**
      * @description Η ηλεκτρονική διεύθυνση με περισσότερες πληροφορίες για το μάθημα
      */
     public courseUrl?: string; 
     
     /**
      * @description Σημειώσεις για το μάθημα
      */
     public notes?: string; 
     
     /**
      * @description Στις περιπτώσεις κατά τις οποίες το μάθημα αντικαταστάθηκε από κάποιο άλλο, στο πεδίο αυτό καταχωρείται ο κωδικός του νέου μαθήματος που το αντικατέστησε
      */
     public replacedByCourse?: Course; 
     
     /**
      * @description Οι κωδικοί των μαθημάτων που αντικατέστησε διαχωριζόμενοι με κόμμα
      */
     public replacedCourse?: string; 
     
     /**
      * @description Ο μέγιστος αριθμός των αναβαθμολογήσεων που επιτρέπεται στο μάθημα
      */
     public maxNumberOfRemarking?: number; 
     
     /**
      * @description Στις περιπτώσεις κατά τις οποίες το μάθημα αποτελεί μέρος άλλου μαθήματος, στο πεδίο αυτό καταχωρείται ο κωδικός του πατρικού του
      */
     public parentCourse?: Course; 
     
     /**
      * @description Στις περιπτώσεις κατά τις οποίες το μάθημα αποτελεί μέρος άλλου μαθήματος, στο πεδίο αυτό καταχωρείται το ποσοστό συμμετοχής στον τελικό βαθμό του μαθήματος (από 0 έως 1)
      */
     public coursePartPercent?: number; 
     
     /**
      * @description Στις περιπτώσεις κατά τις οποίες το μάθημα αποτελεί μέρος άλλου μαθήματος δηλώνεται αν υπολογίζεται στο βαθμό του πατρικού μαθήματος
      */
     public calculatedCoursePart?: boolean; 
     
     /**
      * @description Ανάλογα με τη διδασκαλία και τον τρόπο βαθμολόγησης ενός μαθήματος γίνεται η κατηγοριοποίησή του βάσει του τύπου δομής. (πχ Απλό μάθημα , μικτό μάθημα, μέρος μαθήματος).
      */
     public courseStructureType?: CourseStructureType|any; 
     
     /**
      * @description Ο τομέας του τμήματος που ανήκει το μάθημα
      */
     public courseSector?: CourseSector|any; 
     
     /**
      * @description Αναφέρεται στον τρόπο διδασκαλίας του μαθήματος (πχ. Εργαστηριακό, Διάλεξη κλπ) 
      */
     public courseCategory?: CourseCategory|any; 
     
     /**
      * @description Μονάδες ECTS
      */
     public ects?: number; 
     
     /**
      * @description Ανήκει στο Ίδρυμα (ΝΑΙ/ΟΧΙ)
      */
     public isLocal?: boolean; 
     
     /**
      * @description Ημερομηνία τελευταίας τροποποίησης
      */
     public dateModified: Date; 
     
     /**
      * @description Υπολογίζεται στους κανόνες δήλωσης (ΝΑΙ/ΟΧΙ)
      */
     public calculatedInRegistration?: boolean; 

}

export = Course;
