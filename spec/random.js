import faker from 'faker';
/**
 * Generate random objects for testing against universis api server
 */
class UniversisRandomGenerator {
    /**
     *
     * @returns {Institute|*}
     */
    institute() {
        const city = faker.address.city();
        return {
            id: 20000,
            name: `University of ${city}`,
            alternateName: `${city.substr(0,1).toUpperCase()}U`,
            instituteType: {
                alternateName: 'university'
            },
            local: true
        }
    }
    /**
     *
     * @returns {Department|*}
     */
    department() {
        return {
            id: 20001,
            name: 'School of Law',
            abbreviation:'LAW',
            currentYear: 2015,
            currentPeriod: 1,
            email: faker.internet.url(),
            url: faker.internet.email('Law', 'School'),
            studyLevel: 1,
            address: faker.address.streetAddress()
        }
    }
}

const random = new UniversisRandomGenerator();

module.exports = random;
