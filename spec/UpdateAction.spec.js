import app from '../server/app';
// eslint-disable-next-line no-unused-vars
import {ExpressDataApplication, ExpressDataContext} from "@themost/express";
import {TestUtils} from '../server/utils';
import RequestAction from "../server/models/request-action-model";
import UpdateAction from "../server/models/update-action-model";
const executeInTransaction = TestUtils.executeInTransaction;

describe('UpdateAction', () => {
    /**
     * @type {ExpressDataContext}
     */
    let context;
    beforeAll(done => {
        /**
         * @type {ExpressDataApplication}
         */
        const app1 = app.get(ExpressDataApplication.name);
        context = app1.createContext();
        return done();
    });
    afterAll( done => {
        if (context) {
            return context.finalize( ()=> done() );
        }
        return done();
    });

    it('should add action initiator', async() => {
        await executeInTransaction(context, async () => {
            const newAction = {
                name: 'A request action',
                actionStatus: {
                    alternateName: 'ActiveActionStatus'
                }
            }
            await context.model(RequestAction).silent().save(newAction);
            /**
             * @type {UpdateAction}
             */
            let updateAction = {
                name: 'An update action',
                actionStatus: {
                    alternateName: 'ActiveActionStatus'
                },
                initiator: newAction
            };
            await context.model(UpdateAction).silent().save(updateAction);
            // get action
            updateAction = await context.model(UpdateAction)
                .where('id').equal(updateAction.id)
                .expand('initiator')
                .silent()
                .getItem();
            expect(updateAction.initiator).toBeTruthy();
            expect(updateAction.initiator.id).toBe(newAction.id);
        });
    });

});
