import app from '../server/app';
// eslint-disable-next-line no-unused-vars
import {ExpressDataApplication, ExpressDataContext} from "@themost/express";
import {TranslateService} from "../server/services/translate-service";
describe('TranslateService', () => {
    /**
     * @type {ExpressDataContext}
     */
    let context;
    beforeAll(done => {
        /**
         * @type {ExpressDataApplication}
         */
        const app1 = app.get(ExpressDataApplication.name);
        context = app1.createContext();
        return done();
    });
    afterAll(done => {
        if (context) {
            return context.finalize(() => done());
        }
        return done();
    });
    it('should get service', async() => {
       const translateService = context.application.getStrategy(TranslateService);
       expect(translateService).toBeTruthy();
    });
    it('should get TranslateService.defaultLocale', async() => {
        const translateService = context.application.getStrategy(TranslateService);
        const defaultLocale = context.application.getConfiguration().getSourceAt('settings/i18n/defaultLocale');
        expect(defaultLocale).toBeTruthy();
        expect(translateService.defaultLocale).toBe(defaultLocale);
    });
    it('should use TranslateService.translate(string)', async() => {
        /**
         * get service
         * @type {TranslateService}
         */
        const translateService = context.application.getStrategy(TranslateService);
        translateService.defaultLocale = 'en';
        let translation = translateService.translate('Specialization.CoreName');
        expect(translation).toBe('CORE');
        // change default locale
        translateService.defaultLocale = 'el';
        translation = translateService.translate('Specialization.CoreAbbreviation');
        expect(translation).toBe('ΚΟΡ');
    });

    it('should use TranslateService.setTranslation()', async() => {
        /**
         * get service
         * @type {TranslateService}
         */
        const translateService = context.application.getStrategy(TranslateService);
        translateService.defaultLocale = 'en';
        translateService.setTranslation('en', {
            'HelpTitle': 'Need help?'
        });
        let translation = translateService.get('HelpTitle');
        expect(translation).toBe('Need help?');
        translateService.setTranslation('en', {
            'Help': {
                'Title': 'Need help?'
            }
        });
        translation = translateService.get('Help.Title');
        expect(translation).toBe('Need help?');
    });

    it('should use TranslateService.set(string, string)', async() => {
        /**
         * get service
         * @type {TranslateService}
         */
        const translateService = context.application.getStrategy(TranslateService);
        translateService.defaultLocale = 'en';
        translateService.set('HelloMessage', 'Hello World');
        let translation = translateService.get('HelloMessage');
        expect(translation).toBe('Hello World');
    });
    it('should use TranslateService.set(string, string, string)', async() => {
        /**
         * get service
         * @type {TranslateService}
         */
        const translateService = context.application.getStrategy(TranslateService);
        translateService.defaultLocale = 'en';
        translateService.set('HelloMessage', 'Καλημέρα κόσμε', 'el');
        translateService.defaultLocale = 'el';
        let translation = translateService.get('HelloMessage');
        expect(translation).toBe('Καλημέρα κόσμε');
    });
});
